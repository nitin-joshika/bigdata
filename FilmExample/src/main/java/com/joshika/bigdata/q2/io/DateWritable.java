package com.joshika.bigdata.q2.io;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.hadoop.io.WritableComparable;


public class DateWritable implements WritableComparable<DateWritable> {
	
	private Date date;
	
	public DateWritable() {};
	
	public DateWritable(String date) {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try { 
			this.date = dateFormat.parse(date);
		} catch (ParseException pEx) {
			System.err.println("Error in parsing date for DateWritable = " + date);
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		out.writeUTF(dateFormat.format(this.date));
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		String date = in.readUTF();
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try { 
			this.date = dateFormat.parse(date);
		} catch (ParseException pEx) {
			System.err.println("Error in parsing date for readFields = " + date);
			this.date = Calendar.getInstance().getTime();
		}
	}

	@Override
	public int compareTo(DateWritable dateWritable) {
		return this.date.compareTo(dateWritable.date);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DateWritable other = (DateWritable) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

	@Override
	public String toString() {
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		return dateFormat.format(this.date);
	}
	
	
}
