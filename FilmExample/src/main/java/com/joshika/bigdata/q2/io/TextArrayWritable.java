package com.joshika.bigdata.q2.io;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class TextArrayWritable extends ArrayWritable {
	public TextArrayWritable(Class<? extends Writable> valueClass,
			Writable[] values) {
		super(valueClass, values);
	}

	public TextArrayWritable(Class<? extends Writable> valueClass) {
		super(valueClass);
	}

	@Override
	public Text[] get() {
		return (Text[]) super.get();
	}

	@Override
	public void write(DataOutput arg0) throws IOException {
		for (Text text : get()) {
			text.write(arg0);
		}
	}
	
	@Override
	public String toString() {
	  return Arrays.toString(get());
	}
}
