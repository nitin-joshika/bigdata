package com.joshika.bigdata.q2.mapper;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.joshika.bigdata.q2.io.DateWritable;

public class MovieDORMapper extends Mapper<LongWritable, Text, DateWritable, Text> {

	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {
		String regEx = "(([0-9])|([0-2][0-9])|([3][0-1]))[-](Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[-]\\d{4}";
		
		String line = value.toString();
		String[] breaks = line.split("\\|");
		
		String movieName = breaks[1];
		String date = breaks[2];
		
		Integer year = Integer.parseInt(context.getConfiguration().get("YEAR2LOOKUP"));

		if (date != null && !"".equals(date.trim())) {
			if (date.matches(regEx)) {
				date = date.trim();
				Integer movieYear = Integer.parseInt(date.substring(date.lastIndexOf("-") + 1));
				if (movieYear.equals(year))
					context.write(new DateWritable(date), new Text(movieName));
			}
		}
	}
}