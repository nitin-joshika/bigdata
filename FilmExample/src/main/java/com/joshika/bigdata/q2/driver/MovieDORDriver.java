package com.joshika.bigdata.q2.driver;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.joshika.bigdata.q2.io.DateWritable;
import com.joshika.bigdata.q2.io.TextArrayWritable;
import com.joshika.bigdata.q2.mapper.MovieDORMapper;
import com.joshika.bigdata.q2.reducer.MovieDORReducer;

public class MovieDORDriver {
	public static void main(String[] args) throws Exception {
		if (args.length != 3) {
			System.err.println("Proper arguments required");
			System.exit(1);
		}
		
		Job job = new Job();
		job.setJarByClass(MovieDORDriver.class);
		job.setJobName("Movie");
		
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		
		job.setMapperClass(MovieDORMapper.class);
		job.getConfiguration().set("YEAR2LOOKUP", args[2]);
		job.setReducerClass(MovieDORReducer.class);
		
		job.setMapOutputKeyClass(DateWritable.class);
		job.setMapOutputValueClass(Text.class);
		
		job.setOutputKeyClass(DateWritable.class);
		job.setOutputValueClass(TextArrayWritable.class);
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}

}
