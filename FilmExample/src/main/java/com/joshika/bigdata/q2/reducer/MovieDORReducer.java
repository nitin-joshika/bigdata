package com.joshika.bigdata.q2.reducer;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import com.joshika.bigdata.q2.io.DateWritable;
import com.joshika.bigdata.q2.io.TextArrayWritable;

public class MovieDORReducer extends Reducer<DateWritable, Text, DateWritable, TextArrayWritable> {

	@Override
	protected void reduce(DateWritable key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		ArrayList<Text> movies = new ArrayList<Text>();
		for (Text movie : values) {
				movies.add(new Text(movie));
		}

		context.write(
				key,
				new TextArrayWritable(Text.class, movies.toArray(new Text[movies.size()])));
	}

}
